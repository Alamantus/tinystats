<?php
function fillContents($contents) {
  return preg_replace_callback("/([ \t]*)(?:echo )?require_once\('([a-zA-Z\/]+\.[a-z]+)'\);/", function ($matches) {
  // return preg_replace_callback("/require_once\('([a-z]+\.[a-z]+)'\);/", function ($matches) {
    $srcFolder = __DIR__ . '/src/';
    $spaces = $matches[1];
    $fileName = $matches[2];
    $fileContents = file_get_contents($srcFolder . $fileName);

    $ext = explode('.', $fileName)[1];
    if ($ext === 'php') {
      $fileContents = fillContents(str_replace('<?php' . PHP_EOL, PHP_EOL, $fileContents));
    } else {
      $heredoc = strtoupper($ext);
      $fileContents = $spaces . 'echo <<<' . $heredoc . PHP_EOL . trim($fileContents) . PHP_EOL . $heredoc . ';';
    }
    return $fileContents;
  }, $contents);
}

$file = fillContents("require_once('index.php');");
$file = str_replace(PHP_EOL . PHP_EOL, PHP_EOL, $file);
file_put_contents('tinystats.php', '<?php' . PHP_EOL . $file);
