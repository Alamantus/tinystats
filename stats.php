<?php
define('TINYSTATS_ADMIN_FILENAME', './.tinystatsadmin');
define('TINYSTATS_DB_FILENAME', './tinystats.sqlite3');

function TinyStats_basicAuth($success) {
  $loggedIn = file_exists(TINYSTATS_ADMIN_FILENAME)
    && isset($_SERVER['PHP_AUTH_USER'])
    && isset($_SERVER['PHP_AUTH_PW'])
    && password_verify("{$_SERVER['PHP_AUTH_USER']}:{$_SERVER['PHP_AUTH_PW']}", file_get_contents(TINYSTATS_ADMIN_FILENAME));
  if (!$loggedIn) {
    header('WWW-Authenticate: Basic realm="Stats"');
    header('HTTP/1.1 401 Unauthorized');
    echo 'Access denied!';
    exit;
  } else {
    $success();
  }
}

function TinyStats_install() {
  if (file_exists(TINYSTATS_DB_FILENAME)) {
    // back up existing db
    $backupName = TINYSTATS_DB_FILENAME . '.backup';
    $maxlength = 80;
    while (file_exists($backupName) && strlen($backupName) < $maxlength) {
      // move existing backups to have more and more '.backups' on them
      $backupBackupName = $backupName . '.backup';
      rename($backupName, $backupBackupName);
      $backupName = $backupBackupName;
    }
    if (strlen($backupName) > $maxlength) {
      header('HTTP/1.1 409 Conflict');
      echo 'Too many backup files. Delete some old ones.';
      exit;
    }
    rename(TINYSTATS_DB_FILENAME, TINYSTATS_DB_FILENAME . '.backup');
  }

  try {
    $pdo = new PDO('sqlite:' . TINYSTATS_DB_FILENAME);
    // I should put admin details in here instead - one table for all settings including login
    $pdo->exec("CREATE TABLE counts (domain TEXT NOT NULL, type TEXT NOT NULL, date TEXT NOT NULL, count INTEGER DEFAULT 0, other TEXT);");
    $pdo->exec("CREATE INDEX idx_counts_type ON counts (domain, type);");
    $pdo->exec("CREATE TABLE logs (moment REAL DEFAULT(strftime('%s') || substr(strftime('%f'), 3)), ip TEXT, domain TEXT NOT NULL, path TEXT NOT NULL, query TEXT);");
    $pdo->exec("CREATE INDEX idx_logs_ip ON logs (ip);");
    $pdo->exec("CREATE INDEX idx_logs_request_path ON logs (domain, request_path);");
  } catch (\Exception $ex) {
    header('HTTP/1.1 ' . $ex->getCode());
    echo '<p>' . $ex->getMessage() . '</p>';
    echo '<pre><code>' . $ex->getTraceAsString() . '</code></pre>';
    echo '<p><a href="' . $_SERVER['REQUEST_URI'] . '">Go back</a></p>';
    exit;
  }

  header('HTTP/1.1 201 Created');
  echo '<p>Installed!</p>';
}

function TinyStats_store() {
  if (!TinyStats_shouldLog()) {
    header('HTTP/1.1 429 Too Many Requests');
    exit;
  }
  $pdo = new PDO('sqlite:' . TINYSTATS_DB_FILENAME);
  $now = new DateTimeImmutable;
  // DateTime::createFromFormat('U.u', microtime(true));
  // echo $now->format("m-d-Y H:i:s.u");
  $year = $now->format('Y');
  $month = $now->format('Y-m');
  $day = $now->format('Y-m-d');
  $sql = '';
  foreach(['year', 'month', 'day'] as $type) {
    $pdo->exec("INSERT INTO counts(type, date) SELECT '{$type}', '{$$type}' WHERE NOT EXISTS(
        SELECT 1 FROM counts WHERE type='{$type}' AND date='{$$type}'
    );");
    $pdo->exec("UPDATE counts SET count=count+1 WHERE type='{$type}' AND date='{$$type}';");
  }
  $stmt->prepare('INSERT INTO logs (ip, domain, request_path, request_query) VALUES (?, ?, ?, ?)');
  $stmt->execute([
    $_SERVER['REMOTE_ADDR'],
    isset($_GET['d']) ? $_GET['d'] : $_SERVER['REMOTE_HOST'],
    isset($_GET['p']) ? $_GET['p'] : $_SERVER['REQUEST_URI'],
    isset($_GET['q']) ? $_GET['q'] : $_SERVER['QUERY_STRING'],
  ]);
}

function TinyStats_shouldLog() {
    $pdo = new PDO('sqlite:' . TINYSTATS_DB_FILENAME);
    $ip = $_SERVER['REMOTE_ADDR'];
    $stmt = $pdo->prepare('SELECT moment FROM logs ' . (isset($ip) ? 'WHERE ip=?' : '') . ' ORDER BY moment DESC LIMIT 1;');
    $stmt->execute(array_filter([$ip]));
    $val = $stmt->fetch();
    if ($val) {
        // Don't log requests within 1/10th of a moment
        return microtime(true) - doubleval($val->moment) > 0.1;
    }
    return true;
}

function TinyStats_getCounts($domain = null) {
  $pdo = new PDO('sqlite:' . TINYSTATS_DB_FILENAME);
  $now = new DateTimeImmutable;
  // DateTime::createFromFormat('U.u', microtime(true));
  // echo $now->format("m-d-Y H:i:s.u");
  $year = $now->format('Y');
  $month = $now->format('Y-m');
  $day = $now->format('Y-m-d');
  $sql = "SELECT * FROM counts WHERE ((type='year' AND date='{$year}') OR (type='month' AND date='{$month}') OR (type='day' AND date='{$day}'))";
  if ($domain) {
    $stmt = $pdo->prepare($sql . ' AND domain=?');
    $stmt->execute($domain);
  } else {
    $stmt = $pdo->query($sql);
  }
  return $stmt->fetchAll();
}

function TinyStats_renderView() {
  $pageTitle = isset($_GET['view']) ? ucfirst($_GET['view']) : 'Views';
  $head = <<<HTML
  <html>
  <head>
    <title>TinyStats | {$pageTitle}</title>
    <style>
      html {
        font-family: BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
        margin: 0;
        padding: 0;
      }
      html, button, input {
        font-size: 12pt;
        background: #fff;
        color: #000;
      }
      body { padding: 1rem 1.5rem 2rem; }
      :focus { outline: 2px solid #000; }
      * {
        box-sizing: border-box;
        position: relative;
        max-width: 100vw;
      }
      * + *, p { margin: .5rem 0; }
      label { display: block; }
      input {
        padding: .5rem;
        border: 1px solid #000;
        border-radius: 3px;
      }
      h1 {
        font-weight: bold;
        margin: .5rem 0;
        font-size: 1.8rem;
      }
    </style>
  </head>
  <body>
HTML;
  $body = '<p>No Content</p>';
  $foot = <<<HTML
  </body>
  </html>
HTML;
  switch ($_GET['view'] ?? null) {
    default: {
      $counts = TinyStats_getCounts($_GET['d'] ?? null);
      $byDomain = [];
      foreach($counts as $row) {
        if (!isset($byDomain[$row->domain])) {
          $byDomain[$row->domain] = [];
        }
        $byDomain[$row->domain][$row->type] = [
          'date' = $row->date,
          'visits' => $row->count,
        ];
      }
      // build body html here.
    }
  }
  header('Content-Type: text/html; charset=UTF-8');
  echo $head . $body . $foot;
  exit;
}

// If this is loading itself (i.e. not included in another file)
if (str_replace("\\","/",$_SERVER["SCRIPT_FILENAME"]) === str_replace("\\","/",__FILE__)) {
  switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST': {
      if (file_exists(TINYSTATS_ADMIN_FILENAME)) {
        header('HTTP/1.1 403 Forbidden');
        echo 'Credentials have already been set!';
        exit;
      }
      try {
        file_put_contents(TINYSTATS_ADMIN_FILENAME, password_hash("{$_POST['username']}:{$_POST['password']}", PASSWORD_DEFAULT));
        TinyStats_install();
      } catch (\Exception $ex) {
        header('HTTP/1.1 ' . $ex->getCode());
        echo '<p>' . $ex->getMessage() . '</p>';
        echo '<pre><code>' . $ex->getTraceAsString() . '</code></pre>';
        echo '<p><a href="' . $_SERVER['REQUEST_URI'] . '">Go back</a></p>';
      }
      exit;
    }
    case 'PUT': {
      TinyStats_store();
      exit;
    }
  }

  // GET request method
  if (!file_exists(TINYSTATS_ADMIN_FILENAME)) {
    header('Content-Type: text/html; charset=UTF-8');
    echo <<<HTML
<html>
<head>
  <title>TinyStats | Set Up</title>
  <style>
    html {
      font-family: BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
      margin: 0;
      padding: 0;
    }
    html, button, input {
      font-size: 12pt;
      background: #fff;
      color: #000;
    }
    body { padding: 1rem 1.5rem 2rem; }
    :focus { outline: 2px solid #000; }
    * {
      box-sizing: border-box;
      position: relative;
      max-width: 100vw;
    }
    * + *, p { margin: .5rem 0; }
    label { display: block; }
    input {
      padding: .5rem;
      border: 1px solid #000;
      border-radius: 3px;
    }
    h1 {
      font-weight: bold;
      margin: .5rem 0;
      font-size: 1.8rem;
    }
  </style>
</head>
<body>
  <h1>Welcome to Stats!</h1>
  <p>Please set your username and password for managing Stats:</p>
  <form action="{$_SERVER['REQUEST_URI']}" method="POST">
  <label>Username <input name=username></label>
  <label>Password <input name=password type=password></label>
  <p><strong>These values are CASE SENSITIVE so please keep track of what you enter here!</strong></p>
  <p><em>The only way to change your password is to delete the <code>.statsadmin</code> file to display this form again.</em></p>
  <button type=submit>Set Credentials</button>
  </form>
</body>
</html>
HTML;
    exit;
  }

  switch ($_GET['tinystats_action_route'] ?? null) {
    case 'reinstall': {
      TinyStats_basicAuth(function () {
        TinyStats_install();
      });
      exit;
    }
    case 'view': {
      TinyStats_basicAuth(function () {
        TinyStats_renderView();
      });
      exit;
    }
  }

  header('Content-Type: application/javascript');
  echo <<<JS
(function(location) {
  fetch(`{$_SERVER['REQUEST_URI']}?d=${encodeURIComponent(location.origin)}p=${encodeURIComponent(location.pathname)}&q=${encodeURIComponent(location.search)}`, { method: 'PUT' });
})(window.location);
JS;
}
