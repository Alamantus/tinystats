function TinyStats(extraDataMethod) {
  this.getExtraData = typeof extraDataMethod === 'function' ? extraDataMethod : () => null;

  this.report = function(overrideExtraData) {
    const { host, pathname, searchParams, hash } = new URL(window.location.href);
    const params = Array.from(searchParams.entries());
    const query = params.length > 0 ? params.reduce((result, param) => ({ ...result, [param[0]]: param[1] }), {}) : null;
    const data = {
      host,
      pathname,
      query,
      hash,
    };
    extraData = typeof overrideExtraData !== 'undefined' ? overrideExtraData : this.getExtraData();
    if (extraData) {
      data.extraData = extraData;
    }

    return fetch('{$reportingUrl}', {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "omit",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      referrerPolicy: "origin-when-cross-origin",
      body: JSON.stringify(data),
    });
  }
}
