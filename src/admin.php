<?php
$adminPass = '';
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
  header('Content-Type: text/html; charset=UTF-8');
  if (empty($adminPass)) {
    echo require_once('views/setup.html');
  } else {
    $loggedIn = !empty($adminPass)
        && isset($_SERVER['PHP_AUTH_PW'])
        && password_verify($_SERVER['PHP_AUTH_PW'], $adminPass);
    if (!$loggedIn) {
      header('WWW-Authenticate: Basic realm="TinyStats"');
      header('HTTP/1.1 401 Unauthorized');
      echo 'Access denied!';
    } else {
      $pdo = new PDO('sqlite:' . $dbName);
      $stmt = $pdo->prepare('SELECT * FROM logs ORDER BY moment DESC;');
      $stmt->execute();
      $logs = $stmt->fetchAll();
      $logs = '<pre><code>' . var_export($logs, true) . '</code></pre>';
      echo require_once('views/admin.html');
    }
  }
  exit;
}

if (empty($adminPass)) {
  if (isset($_POST['password']) && !empty($_POST['password'])) {
    $file = file_get_contents(__FILE__);

    // Edit the variables within the code itself
    $pwHash = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $file = str_replace('$adminPass = ' . "'';", "\$adminPass = '{$pwHash}';", $file); // Split the string up so it doesn't match itself
    $dbFilename = preg_replace('/[^\w\.\-\_]/', '', ($_POST['dbName'] ?? 'tinystats.sqlite3')); // Remove anything from dbName that's not alphanumeric or . - _
    $file = preg_replace("/\\\$dbName = '[^']*';/", "\$dbName = '{$dbFilename}';", $file);
    $domains = array_map(function ($domain) {
      return "'" . preg_replace('/[^a-z0-9\.\-]/', '', strtolower($domain)) . "'"; // No international or special characters
    }, explode(PHP_EOL, $_POST['domains']));
    $file = preg_replace("/\\\$allowedDomains = \[[^\(\)\]]*\];/", '$allowedDomains = [' . implode(',', $domains) . '];', $file);
    file_put_contents(__FILE__, $file);

    if (file_exists($dbFilename)) {
      // back up existing db
      $backupName = $dbFilename . '.backup';
      $maxlength = 80;
      while (file_exists($backupName) && strlen($backupName) < $maxlength) {
          // move existing backups to have more and more '.backups' on them
          $backupBackupName = $backupName . '.backup';
          rename($backupName, $backupBackupName);
          $backupName = $backupBackupName;
      }
      if (strlen($backupName) > $maxlength) {
          header('HTTP/1.1 409 Conflict');
          echo 'Too many backup files. Delete some old ones.';
          exit;
      }
      rename($dbFilename, $dbFilename . '.backup');
    }

    $pdo = new PDO('sqlite:' . $dbFilename);
    $pdo->exec("CREATE TABLE logs (`domain` TEXT NOT NULL, `path` TEXT NOT NULL, `query` TEXT, `hash` TEXT, `extra` TEXT, `ip_hash` TEXT, `session` TEXT, `moment` REAL DEFAULT(strftime('%s') || substr(strftime('%f'), 3)));");
    $pdo->exec("CREATE INDEX idx_logs_ip_hash ON logs (`ip_hash`);");
    $pdo->exec("CREATE INDEX idx_logs_session ON logs (`ip_hash`, `session`);");
    $pdo->exec("CREATE INDEX idx_logs_domain ON logs (`domain`);");
    $pdo->exec("CREATE INDEX idx_logs_path ON logs (`domain`, `path`);");
  }
  header('Location: ' . $_SERVER['REQUEST_URI']);
  exit;
}

header('HTTP/1.1 405 Method Not Allowed');
echo 'Already set up';
