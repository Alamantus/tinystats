<?php

$dbName = 'tinystats.sqlite3';

if (isset($_GET['admin'])) {
  // Handle the setup and admin separately
  require_once('admin.php');
  exit;
}

$allowedDomains = [];

// Handle CORS for allowed domains
if (isset($_SERVER['HTTP_ORIGIN'])) {
  $originUrl = parse_url($_SERVER['HTTP_ORIGIN']);
  if (in_array($originUrl['host'], $allowedDomains)) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
  }
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
  }
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
  }
  exit;
}

// If it's any request type other than POST, return the JavaScript
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
  $host = $_SERVER['HTTP_HOST'];
  $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
  $reportingUrl = "{$protocol}://{$host}{$_SERVER['REQUEST_URI']}";
  // header('Access-Control-Max-Age: 86400');    // cache for 1 day
  header('Content-Type: text/javascript; charset=UTF-8');
  echo require_once('index.js');
  exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

// Check if it can log
if (!isset($data['host']) || empty($data['host'])) {
  header('HTTP/1.1 400 Bad Request');
  exit;
}

$host = trim($data['host']);
if (!in_array($host, $allowedDomains)) {
  header('HTTP/1.1 401 Unauthorized');
  exit;
}

// Check if it should log
$pdo = new PDO('sqlite:' . $dbName);
$ip = $_SERVER['REMOTE_ADDR'];
$ipHash = isset($ip) ? md5($ip) : null;
$stmt = $pdo->prepare('SELECT moment FROM logs ' . (isset($ip) ? 'WHERE ip_hash=?' : '') . ' ORDER BY moment DESC LIMIT 1;');
$stmt->execute(isset($ip) ? [$ipHash] : []);
$val = $stmt->fetch();
// Don't log requests within 1/10th of a moment
if ($val && (microtime(true) - doubleval($val['moment']) < 0.1)) {
    header('HTTP/1.1 429 Too Many Requests');
    exit;
}

// Log it!
$pdo = new PDO('sqlite:' . $dbName);
$stmt = $pdo->prepare(<<<SQL
  INSERT INTO logs(`domain`, `path`, `query`, `hash`, `extra`, `ip_hash`, `session`)
  VALUES(?, ?, ?, ?, ?, ?, ?)
SQL);
$stmt->execute([
  $host,
  isset($data['pathname']) && !empty(trim($data['pathname'])) ? trim($data['pathname']) : '/',
  isset($data['query']) && !empty($data['query']) ? trim($data['query']) : null,
  isset($data['hash']) && !empty(trim($data['hash'])) ? trim($data['hash']) : null,
  isset($data['extraData']) && !empty($data['extraData']) ? json_encode($data['extraData']) : null,
  $ipHash,
  null, // Figure out a better way to determine individual user on same IP
]);
